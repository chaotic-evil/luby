#![no_std]
#![warn(missing_debug_implementations)]

use num_traits::{int::PrimInt, ops::wrapping::WrappingNeg};

#[derive(Clone, Debug)]
pub struct Luby<T: PrimInt + WrappingNeg>(T, T);

impl<T: PrimInt + WrappingNeg> Luby<T> {
    #[allow(clippy::new_without_default)]
    // Default doesn't make much sense here.
    /// Initialises a new Luby sequence.
    pub fn new() -> Luby<T> {
        // Initial values picked so that iterating yields 1, 1, 2, ...
        Luby(T::zero(), T::zero())
    }
}

impl<T: PrimInt + WrappingNeg> Iterator for Luby<T> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        let (u, v) = (&mut self.0, &mut self.1);

        if *u & u.wrapping_neg() == *v {
            // Reset the sequence: u += 1, v = 1
            *u = u.checked_add(&T::one())?;
            *v = T::one();
        } else {
            // Double v
            *v = v.checked_add(v)?;
        }

        Some(*v)
    }
}

#[cfg(test)]
mod tests {
    use crate::generic_test;
    use core::fmt::Debug;
    use num_traits::{int::PrimInt, ops::wrapping::WrappingNeg};

    /// `Luby<T>` only takes power-of-2 values, and ends the sequence cleanly,
    ///  rather than overflowing.
    fn powers_of_two<T: PrimInt + WrappingNeg>() {
        let luby = super::Luby::<u8>::new();
        for (i, v) in luby.enumerate() {
            #[rustfmt::skip]
            assert!(
                v.count_ones() == 1,
                "{}th term {} = {:#b} is not a power of 2", i, v, v
            );
        }
    }
    generic_test!(powers_of_two, u8, u16, u32, u64, i8, i16, i32, i64);

    /// `Luby<T>` produces a sequence that matches the known prefix
    fn prefix<T: PrimInt + WrappingNeg + Debug>() {
        let luby = super::Luby::<T>::new();

        for (i, j) in luby.zip(LUBY_PREFIX) {
            assert_eq!(i, T::from(*j).unwrap());
        }
    }
    generic_test!(prefix, u8, u16, u32, u64, i8, i16, i32, i64);

    /// Sequence prefix taken from https://oeis.org/A182105
    #[rustfmt::skip]
    const LUBY_PREFIX: &[u8] = &[1, 1, 2,
                                 1, 1, 2, 4,
                                 1, 1, 2,
                                 1, 1, 2, 4, 8,
                                 1, 1, 2,
                                 1, 1, 2, 4,
                                 1, 1, 2,
                                 1, 1, 2, 4, 8, 16,
                                 1, 1, 2,
                                 1, 1, 2, 4,
                                 1, 1, 2,
                                 1, 1, 2, 4, 8,
                                 1, 1, 2,
                                 1, 1, 2, 4,
                                 1, 1, 2,
                                 1, 1, 2, 4, 8, 16, 32,
                                 1, 1, 2,
                                 1, 1, 2, 4,
                                 1, 1, 2,
                                 1, 1, 2, 4, 8,
                                 1, 1, 2,
                                 1, 1, 2, 4,
                                 1, 1, 2,
                                 1, 1, 2, 4, 8, 16,
                                 1, 1, 2,
                                 1, 1, 2, 4,
                                 1, 1, 2,
                                 1, 1, 2, 4, 8];
}

#[cfg(test)]
#[macro_export]
macro_rules! generic_test {
    ($f:ident, $($t:ty),+) => {
        paste::item! {
            $(#[test]
              fn [< $f _ $t >]() {
                  $f::<$t>()
              })+
        }
    };
}
